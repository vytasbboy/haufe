import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs';

@Injectable()
export class HttpService {

    private apiUrl = 'http://localhost:3000/api';

    constructor(private http: Http) {
    }

    public get<T>(url: string): Observable<T> {
        return this.http.get(`${this.apiUrl}/${url}`)
            .map((res: Response) => {
               return res.json();
            })
            .catch((error: any) => {
                return Observable.throw(error.json().error || 'Server error');
            });
    }
}