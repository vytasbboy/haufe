import { Routes } from '@angular/router';
import { TodosComponent } from './components/todos.component';
import { TodoDetailsComponent } from './components/todo-details.component';

export const routes: Routes = [
  {
    path: '', redirectTo: 'todos', pathMatch: 'full'
  },
  {
    path: 'todos',
    component: TodosComponent,
  },
  {
    path: 'todos/:id',
    component: TodoDetailsComponent
  }
];
