import { AppComponent } from './app';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { routes } from './routes';
import { ComponentModule } from './components/component.module';
import { ServicesModule } from './services/services.module';

@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        RouterModule.forRoot(routes, { useHash: true }),
        ComponentModule,
        ServicesModule
    ],
    declarations: [ AppComponent ],
    bootstrap: [ AppComponent]
})
export class AppModule {
}