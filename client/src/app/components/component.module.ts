import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TodosComponent } from './todos.component';
import { TodoDetailsComponent } from './todo-details.component';

@NgModule({
    imports: [ CommonModule, RouterModule ],
    declarations: [
        TodosComponent,
        TodoDetailsComponent
    ]
})
export class ComponentModule {
}