import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { HttpService } from '../services/http.service';
import { TodoModel } from './todo.model';
import { Observable } from 'rxjs/Observable';

@Component({
    selector: 'todo-details',
    changeDetection: ChangeDetectionStrategy.OnPush,
    styles: [
        'a { color: white };',
        'a:hover { color: #5bc0de };',
        '.info { min-height: 102px; };'
    ],
    template: `
    <div class="container">
        <h4>Details</h4>
        <br />
        <div class="card-deck">
            <div class="card" style="width: 20rem;">
                <div class="card-block info">
                    <h4 class="card-title">{{ (todoItem | async)?.name }}</h4>
                    <p class="card-text">{{ (todoItem | async)?.description }}</p>
                </div>
                <div class="card-block card-primary href-block">
                    <a href="#" [routerLink]="['/todos']" class="card-link">Back</a>
                </div>
            </div>
        </div>
    </div>
  `
})
export class TodoDetailsComponent implements OnInit {
    public todoItem: Observable<TodoModel>;

    private itemId: number;

    constructor(private route: ActivatedRoute, private httpService: HttpService) {
        this.route.params.subscribe((params: Params) => {
            this.itemId = params.id;
        });
    }

    ngOnInit() {
        this.todoItem = this.httpService.get(`todos/${this.itemId}`);
    }
}