import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { TodoModel } from './todo.model';
import { HttpService } from '../services/http.service';

@Component({
    selector: 'todos',
    changeDetection: ChangeDetectionStrategy.OnPush,
    styles: [
        'thead tr { background-color: rgb(2,117,216); color: white; }',
        'tbody tr:nth-child(even) { background-color: rgba(2,117,216, 0.4); color: white; }',
    ],
    template: `
    <div class="container">
        <h4>Todo items</h4>
        <br />
        <table class="table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <tr *ngFor="let todo of todos | async; let i = index; trackBy: trackById;">
                    <th scope="row">{{ i + 1 }}</th>
                    <td>{{ todo.name }}</td>
                    <td><a href="#" [routerLink]="[todo.id]" class="card-link">View Details</a></td>
                </tr>
            </tbody>
        </table>
    </div>
    `
})
export class TodosComponent implements OnInit {
    public todos: Observable<TodoModel[]>;

    constructor(private httpService: HttpService) {
    }

    ngOnInit() {
        this.todos = this.httpService.get('todos');
    }

    trackById(item: TodoModel) {
        return item.id;
    }
}