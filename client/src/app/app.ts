import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'haufe-app',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styles: [
    '.container {margin-top: 30px}',
  ],
  template: `
      <div class="container">
        <router-outlet></router-outlet>
      </div>
  `
})
export class AppComponent {
}