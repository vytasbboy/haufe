import * as restify from 'restify';

export default class TodoController {

    private todoList: ITodoItem[] = [
        {
            id: 1,
            name: 'Item 1',
            description: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, '
        },
        {
            id: 2,
            name: 'Item 2',
            description: 'Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero'
        },
        {
            id: 3,
            name: 'Item 3',
            description: 'Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo'
        },
        {
            id: 4,
            name: 'Item 4',
            description: 'Description for item 4'
        }
    ];

    public getTodos(req: restify.Request, res: restify.Response, next: restify.Next) {
        res.json(200, this.todoList.map(item => {
            const { id, name } = item;
            return { id, name };
        }));
        next();
    }

    public getTodo(req: restify.Request, res: restify.Response, next: restify.Next) {
        if (this.isRequestValid(req, res)) {
            const todoItem = this.todoList.find(item => item.id === parseInt(req.params.id));
            res.json(200, todoItem);
        }

        next();
    }

    private isRequestValid(req: restify.Request, res: restify.Response) {
        if (!req.params.id) {
            res.json(400, 'No `id` was provided');
            return false;
        }

        return true;
    }
}

interface ITodoItem {
    id: number;
    name: string;
    description: string;
}