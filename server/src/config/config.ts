let env = process.env.NODE_ENV || 'development';

export let settings: Config = {
    name: 'Haufe Todo Api',
    version: '1.0.0',
    port: 3000,
    env: 'dev'
};

if (env === 'production') {
    settings.env = 'prod';
    // other production settings
}

export interface Config {
    name: string;
    port: number;
    env: string;
    version: string;
}