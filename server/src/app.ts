import * as fs from 'fs';
import * as restify from 'restify';
import { settings } from './config/config';

export let server = restify.createServer({
    name: settings.name
});

server.use((<any>restify).plugins.fullResponse());

fs.readdirSync(__dirname + '/routes').forEach(function (routeConfig: string) {
    if (routeConfig.substr(-3) === '.js') {
        let route = require(__dirname + '/routes/' + routeConfig);
        route.routes(server);
    }
});

server.listen(settings.port, function () {
    console.info(`INFO: ${settings.name} is running at ${server.url}`);
});