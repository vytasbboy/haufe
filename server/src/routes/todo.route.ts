import * as restify from 'restify';
import TodoController from '../controllers/todo.controller';

function sampleRoute(api: restify.Server) {
    let routeCtrl = new TodoController();
    api.get('api/todos', routeCtrl.getTodos.bind(routeCtrl));
    api.get('api/todos/:id', routeCtrl.getTodo.bind(routeCtrl));
}

module.exports.routes = sampleRoute;